# SPDX-FileCopyrightText: © 2023 Dominik George <nik@naturalnet.de>
#
# SPDX-License-Identifier: Apache-2.0

from twisted.cred.portal import Portal
from twisted.mail.imap4 import IMAP4Server, LOGINCredentials, PLAINCredentials
from twisted.internet.protocol import Factory

from vocata.graph import ActivityPubGraph
from vocata.settings import get_settings


from .realm import VocataBasicAuthChecker, VocataRealm


class VocataIMAPFactory(Factory):
    def __init__(self):
        # Establish link to the Vocata graph store
        self.settings = get_settings()
        self.graph = ActivityPubGraph(
            database=self.settings.graph.database.uri,
            store=self.settings.graph.database.store,
        )

        realm = VocataRealm(self.graph)
        checker = VocataBasicAuthChecker(self.graph)

        self.portal = Portal(realm)
        self.portal.registerChecker(checker)

    def startFactory(self):
        self.graph.open(create=True)

    def stopFactory(self):
        self.graph.close()

    def buildProtocol(self, addr) -> IMAP4Server:
        # FIXME maybe only allow after STARTTLS
        challengers = {b"PLAIN": PLAINCredentials, b"LOGIN": LOGINCredentials}
        proto = IMAP4Server(challengers)

        proto.IDENT = b"Vocata IMAPd ready"
        proto.portal = self.portal
        proto.factory = self

        return proto
