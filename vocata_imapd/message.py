# SPDX-FileCopyrightText: © 2023 Dominik George <nik@naturalnet.de>
#
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime
from email.utils import format_datetime
from io import BytesIO
from typing import Iterable

from rdflib import Literal, URIRef
from twisted.mail.interfaces import IMessageIMAP
from zope.interface import implementer

from vocata.graph import ActivityPubGraph
from vocata.graph.schema import AS, VOC


@implementer(IMessageIMAP)
class ActivityPubMessage:
    def __init__(self, graph: ActivityPubGraph, object_: URIRef, uid: int):
        self.graph = graph
        self.object_ = object_
        self._uid = uid

    def getUID(self) -> int:
        return self._uid

    def getFlags(self) -> Iterable[str]:
        return []

    def getInternalDate(self) -> bytes:
        processed_at = self.graph.value(
            subject=self.object_, predicate=VOC.processedAt, default=Literal(datetime.now())
        )
        return format_datetime(processed_at.value)

    def getHeaders(self, negate: bool, *names) -> dict[str, str]:
        # FIXME generate header fields
        headers = {}

        if subject := self.graph.value(subject=self.object_, predicate=AS.name):
            headers["Subject"] = str(subject)
        elif subject := self.graph.value(subject=self.object_, predicate=AS.summary):
            headers["Subject"] = str(subject)

        headers["Content-Type"] = str(
            self.graph.value(
                subject=self.object_, predicate=AS.mediaType, default=Literal("text/html")
            )
        )

        return headers

    def isMultipart(self) -> bool:
        # FIXME consider attachments
        return False

    def _getContent(self) -> str:
        content = str(
            self.graph.value(subject=self.object_, predicate=AS.content, default=Literal(""))
        )
        return content

    def getBodyFile(self):
        return BytesIO(self._getContent().encode())

    def getSize(self) -> int:
        return len(self._getContent().encode())
