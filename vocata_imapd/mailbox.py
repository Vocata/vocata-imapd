# SPDX-FileCopyrightText: © 2023 Dominik George <nik@naturalnet.de>
#
# SPDX-License-Identifier: Apache-2.0

from rdflib import URIRef
from rdflib.query import Result
from twisted.mail.imap4 import IAccount, IMailbox, MessageSet
from twisted.mail.interfaces import IMailboxIMAPListener
from zope.interface import implementer

from vocata.graph import ActivityPubGraph
from vocata.graph.schema import AS, RDF

from .message import ActivityPubMessage

MESSAGES_QUERY = (
    f"""
    PREFIX as: <{AS}>
    PREFIX rdf: <{RDF}>
"""
    + """
    SELECT DISTINCT ?object
    WHERE {
        ?box as:items/rdf:rest*/rdf:first ?activity .
        ?activity a         as:Create ;
                  as:object ?object .
        ?object a as:Note .
    }
"""
)


@implementer(IMailbox)
class VocataMailbox:
    def __init__(self, graph: ActivityPubGraph, actor: URIRef, box: URIRef):
        self.graph = graph
        self.actor = actor
        self.box = box

        self._listeners = set()

    def getHierarchicalDelimiter(self) -> str:
        return "/"

    def isWriteable(self) -> int:
        return 0

    def getFlags(self) -> list[str]:
        return []

    def _getMessagesResult(self) -> Result:
        return self.graph.query(MESSAGES_QUERY, initBindings={"box": self.box})

    def getMessageCount(self) -> int:
        return len(self._getMessagesResult())

    def getRecentCount(self) -> int:
        return len(self._getMessagesResult())

    def getUIDValidity(self) -> int:
        # FIXME must be unique during session, must be different in next session
        return 123

    def fetch(self, messages: MessageSet, uid: bool):
        # FIXME stabilize (order, etc.)
        messages_res = self._getMessagesResult()
        messages_rows = list(messages_res)

        messages.last = len(messages_res)
        for seqno in messages:
            message = ActivityPubMessage(self.graph, messages_rows[seqno - 1].object, seqno)
            yield (seqno, message)

    def addListener(self, listener: IMailboxIMAPListener):
        self._listeners.add(listener)

    def removeListener(self, listener: IMailboxIMAPListener):
        self._listeners.remove(listener)


@implementer(IAccount)
class VocataIMAPAccount:
    def __init__(self, graph: ActivityPubGraph, actor: URIRef):
        self.graph = graph
        self.actor = actor

    def listMailboxes(self, ref, wildcard):
        # FIXME implement ref and wildcard
        # FIXME account for other streams/mailboxes (sharedInbox?)
        for box in ("INBOX",):
            yield (box, self.select(box))

    def select(self, path: str, rw: bool = False) -> VocataMailbox:
        if path.upper() != "INBOX":
            raise KeyError("only INBOX supported for now")

        inbox = self.graph.get_actor_inbox(self.actor)
        return VocataMailbox(self.graph, self.actor, inbox)
