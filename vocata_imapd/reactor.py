# SPDX-FileCopyrightText: © 2023 Dominik George <nik@naturalnet.de>
#
# SPDX-License-Identifier: Apache-2.0

from twisted.internet import reactor

from .protocol import VocataIMAPFactory


def run_reactor():
    reactor.listenTCP(12143, VocataIMAPFactory())
    reactor.run()
