# SPDX-FileCopyrightText: © 2023 Dominik George <nik@naturalnet.de>
#
# SPDX-License-Identifier: Apache-2.0

from rdflib import URIRef
from twisted.cred.checkers import ICredentialsChecker
from twisted.cred.credentials import IUsernamePassword
from twisted.cred.error import UnauthorizedLogin
from twisted.cred.portal import IRealm
from twisted.mail.imap4 import IAccount
from zope.interface import implementer

from vocata.graph import ActivityPubGraph

from .mailbox import VocataIMAPAccount


@implementer(IRealm)
class VocataRealm:
    def __init__(self, graph: ActivityPubGraph):
        self.graph = graph

    def requestAvatar(self, avatarId: bytes, mind, *interfaces):
        if IAccount not in interfaces:
            raise NotImplementedError("This realm only supports the imap4.IAccount interface.")

        actor = URIRef(avatarId.decode())

        avatar = VocataIMAPAccount(self.graph, actor)
        return (IAccount, avatar, lambda: None)


@implementer(ICredentialsChecker)
class VocataBasicAuthChecker:
    credentialInterfaces = [IUsernamePassword]

    def __init__(self, graph: ActivityPubGraph):
        self.graph = graph

    def requestAvatarId(self, credentials: IUsernamePassword) -> bool:
        username = credentials.username.decode()
        if "@" not in username:
            raise UnauthorizedLogin("user must be in user@domain form")

        actor = self.graph.get_canonical_uri(f"acct:{username}")
        if not actor:
            raise UnauthorizedLogin("invalid credentials")

        password = credentials.password.decode()
        valid = self.graph.verify_actor_password(actor, password)
        if valid:
            return str(actor).encode()
        raise UnauthorizedLogin("invalid credentials")
