# IMAP Server giving access to ActivityPub through Vocata

vocata-imapd is a sample implementation of a custom server working
with [Vocata](https://codeberg.org/Vocata/vocata)'s ActivityPub graph.

## Setup

vocata-imapd uses the same configuration file and store as Vocata itself.
Thus, it is simply started as `vocata-imapd`.

It then starts accepting IMAP connections on port 12143, allows *PLAIN* and
*LOGIN* authentication using credentials set with `vocatactl actor ... set-password`,
and provides access to the authenticated actor's inbox.

## Available data

Being a mere sample, vocata-imapd currently only provides acces to:

* `Note` objects that have a `Create` activity in the actor's inbox

## Demo

Watch my favourite mail client [neomutt](https://neomutt.org/) retrieve an
[example note](https://masto.vocata.one/@admin/110369167709792653)
from my local test server's inbox (freshly received from Mastodon via ActivityPub):

![vocata-imapd first demo](docs/vocata-imapd-first-demo.gif)

## Credits

Shoutouts go out to [Oblomov](https://sociale.network/@oblomov), who
[nerd-sniped me](https://floss.social/@oblomov@sociale.network/110360837453292004)
with an IMAP-to-ActivityPub gateway.
